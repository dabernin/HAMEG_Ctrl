#include "powersupply.h"
//#include "mainwindow.h"
#include <qthread.h>
#include <iostream>
#include <unistd.h>



PowerSupply::PowerSupply()
{
    isOpen = 0;
    //parent = NULL;
}

/*
PowerSupply::PowerSupply(MainWindow *par)
{
    isOpen = 0;
    parent = par;
}
*/

PowerSupply::~PowerSupply()
{
  Close();
}

    //print a formatted string to the PS
void PowerSupply::ps_printf(const char* fmt, ...)
{
    QString result;
    va_list args;
    va_start(args, fmt);
    result.vsprintf(fmt, args);
    va_end(args);

    //char toWrite[result.length()+1];
    //strcpy(toWrite, qPrintable(result));
    //serialPort.write(toWrite, strlen(toWrite));
    serialPort.write(qPrintable(result), result.length());
    serialPort.waitForBytesWritten(500);
}

    //read a CR-LF terminated string from PS
int PowerSupply::ps_read(char* dest, int maxSize)
{
    int ptr = 0;
    while (1)
    {
        if (serialPort.read(dest+ptr,1) <= 0)
        {
            if (!serialPort.waitForReadyRead(20))
                break;                              //no new data - break
        }
        else
        {
            ptr++;
            if (dest[ptr-1] == '\n')
            {
                ptr--;
                break;                                  //newline found - delete and break
            }
            if (dest[ptr-1] == '\r')
                ptr--;                                  //CR found - delete it
            if (ptr >= (maxSize-1))
                break;                                  //buffer full = break
        }
    }

    dest[ptr] = 0;

    return ptr;
    //serialPort.read(dest, maxSize);
}

//Flush the buffer
void PowerSupply::ps_flush() {
  char buffer[32];
  while(ps_read(buffer, 32));
}


    //Finds PS's serial port and opens it
int PowerSupply::Open()
{
    QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();

    Close();    //close port if it's open

    if (list.size() == 0)
        return -1;      //no serial ports found

    for (int port = 0; port < list.size(); port++)
    {
        serialPort.setPort(list[port]);
        serialPort.setBaudRate(57600);
        if (serialPort.open(QIODevice::ReadWrite))
        {
            serialPort.setBaudRate(57600);
            ps_flush();
            ps_printf("*IDN?\n");
            //usleep(100);
            char buffer[128];
            ps_read(buffer,128);

            if (strstr(buffer, "HAMEG,HMP4040"))
            {                                       //gotta our PS!
                isOpen = 1;
                portName = list[port].portName();
                ID = buffer;
                break;
            }
            serialPort.close();
        }
    }
    return isOpen;
}

void PowerSupply::Close()
{
    if (isOpen)
    {
        isOpen = 0;
        serialPort.close();
        std::cout << "Connection to '" << portName.toStdString() << "'closed";
        std::cout << std::endl;
    }
}

void PowerSupply::setVoltage(int channel, double voltage)
{
    if (!isOpen)
        return;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_printf("VOLT %1.3f\n", voltage);
}

//Get voltage set
double PowerSupply::getVoltage(int channel)
{
    if (!isOpen)
        return -1;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_flush();
    ps_printf("VOLT?\n");
    //usleep(200);
    char buffer[128];
    ps_read(buffer,128);
    return strtod(buffer, NULL);
}

//Measure voltage
double PowerSupply::measVoltage(int channel)
{
    if (!isOpen)
        return -1;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_flush();
    ps_printf("MEAS:VOLT?\n");
    //usleep(200);
    char buffer[128];
    ps_read(buffer,128);
    return strtod(buffer, NULL);
}

void PowerSupply::setCurrent(int channel, double current)
{
    if (!isOpen)
        return;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_printf("CURR %1.3f\n", current);
}


//Get current value set (max. current)
double PowerSupply::getCurrent(int channel)
{
    if (!isOpen)
        return -1;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_flush();
    ps_printf("CURR?\n");
    //usleep(200);
    char buffer[128];
    ps_read(buffer,128);
    return strtod(buffer, NULL);
}

//Get measured current
double PowerSupply::measCurrent(int channel)
{
    if (!isOpen)
        return -1;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_flush();
    ps_printf("MEAS:CURR?\n");
    //usleep(200);
    char buffer[128];
    ps_read(buffer,128);
    return strtod(buffer, NULL);
}


//set channel as active
void PowerSupply::activateOutput(int channel, int state)
{
    if (!isOpen)
        return;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_printf("OUTP:SEL %1d\n", state);
}

int PowerSupply::isActivated(int channel)
{
    if (!isOpen)
        return -1;

    ps_printf("INST:SEL OUT%1d\n", channel);
    ps_flush();
    ps_printf("OUTPUT:SEL?\n");
    //usleep(200);
    char buffer[128];
    ps_read(buffer,128);
    return strtol(buffer, NULL, 10);
}

    //turn on/off all active channels
void PowerSupply::power(int on)
{
    if (!isOpen)
        return;

    ps_printf("OUTP:GEN %1d\n", on);
}


int PowerSupply::isPowered()
{
    if (!isOpen)
        return -1;

    ps_flush();
    ps_printf("OUTP:GEN?\n");
    //usleep(200);
    char buffer[128];
    ps_read(buffer,128);
    return strtol(buffer, NULL, 10);
}
