#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <unistd.h>  //usleep
#include "powersupply.h"

void print_usage(std::string name) {
  if(name != "")
    name = " "+name;
  std::cout << "Usage:" << name << " [<command>] [argument(s)]" << std::endl;
  std::cout << "Where <command> is one of the following:" << std::endl;
  std::cout << "isOpen" << std::endl;
  std::cout << "status" << std::endl;
  std::cout << "power [ON/OFF]" << std::endl;
  std::cout << "ch <chnumber(s)> [ON/OFF or <value>A/V]" << std::endl;
  if(name != "") {
    std::cout << "Running it without a command runs ";
    std::cout << "the program in interpreter mode!" << std::endl;
  }
}

bool isCommand(std::string command) {
  bool isComm = false;

  if(command == "isOpen") isComm = true;
  else if(command == "power") isComm = true;
  else if(command == "ch") isComm = true;
  else if(command == "status") isComm = true;

  return isComm;
}

int openport(PowerSupply* hameg) {
  int isOpen = hameg->Open();
  if(isOpen == 1) {
    std::cout << "Connected to '" << hameg->ID << "' on '";
    std::cout << hameg->portName.toStdString() << "'!" << std::endl;
    return 0;
  }
  else if(isOpen == -1) {
    std::cout << "No serial ports found!" << std::endl;
    return 1;
  }
  else {
    std::cout << "Connection could not be established!" << std::endl;
    return 1;
  }
}

//Print status of ch
void chstatus(PowerSupply* hameg, int ch) {
  int active = hameg->isActivated(ch);
  std::string activestr;
  if(active == -1) activestr = "ERROR";
  else activestr = active ? "ON" : "OFF";
  double voltage = hameg->getVoltage(ch);
  double voltagemeas = hameg->measVoltage(ch);
  double current = hameg->getCurrent(ch);
  double currentmeas = hameg->measCurrent(ch);
  std::cout << "Channel " << ch << ": ";
  std::cout.width(3);
  std::cout << activestr << " | ";
  std::cout << "Set: ";
  std::cout.width(5);
  std::cout << voltage << " V & ";
  std::cout.width(5);
  std::cout << current << " A | ";
  std::cout << "Meas: ";
  std::cout.width(5);
  std::cout << voltagemeas << " V & ";
  std::cout.width(5);
  std::cout << currentmeas << " A" << std::endl;
  //std::cout << "   Voltage: " << voltage << "V" << std::endl;
  //std::cout << "   Current: " << current << "A" << std::endl;
}

//Print power status
void powerstatus(PowerSupply* hameg) {
  int powered = hameg->isPowered();
  std::string poweredstr;
  if(powered == -1) poweredstr = "ERROR";
  else poweredstr = powered ? "ON" : "OFF";
  std::cout << "Power: " << poweredstr << std::endl;
}

int powerCommand(PowerSupply* hameg, int argc, std::vector<std::string> argv) {
  int result = 0;  //0 = OK
  if(argc == 2) {
    //Return status of power
    powerstatus(hameg);
  }
  else if(argc == 3) {
    std::string ONOFFstr = argv[2];
    if(ONOFFstr != "ON" && ONOFFstr != "OFF") {
      result = 1;
    }
    else { //OK until here
      int ONOFF = ONOFFstr == "ON" ? 1 : 0;
      hameg->power(ONOFF);
      std::cout << "Hameg powered " << ONOFFstr << std::endl;
      result = 0;
    }
  }
  else result = 1;

  if(result != 0) {
    std::cout << "Power command must be issued as:" << std::endl;
    std::cout << "power [ON/OFF]" << std::endl;
  }

  return result;
}


int chCommand(PowerSupply* hameg, int argc, std::vector<std::string> argv) {
  int result = 0; //0 == OK
  if(argc < 3 || argc > 4 ) {
    result = 1;
  }
  else {
    //Channel can be 123 for channels 123
    int combch = strtol(argv[2].c_str(), NULL, 10);
    std::vector<int> chs;
    while(combch > 0) {
      int ch = combch%10;
      if(ch < 1 or ch > 4) {
        std::cout << "Individual channel must be 1, 2, 3 or 4!" << std::endl;
        return 1;
      }
      chs.insert(chs.begin(),ch);
      combch = combch/10;
    }
    if(argc == 3) {
      //Request status of ch(s)
      for(auto ch : chs) chstatus(hameg, ch);
    }
    else { //Change settings
      std::string setting = argv[3];
      char lastchar = setting.back();

      //ON/OFF command
      if(setting == "ON" || setting == "OFF") {
        int ONOFF = setting == "ON" ? 1 : 0;
        for(auto ch : chs) {
          hameg->activateOutput(ch, ONOFF);
          std::cout << "Set channel " << ch << " " << setting << std::endl;
        }
      }
      //V or A?
      else if(lastchar == 'V') {
        setting.pop_back();
        double voltage = strtod( setting.c_str(), NULL );
        for(auto ch : chs) {
          hameg->setVoltage(ch, voltage);
          std::cout << "Set voltage of channel " << ch << " to " << voltage;
          std::cout << std::endl;
        }
      }
      else if(lastchar == 'A') {
        setting.pop_back();
        double current = strtod( setting.c_str(), NULL );
        for(auto ch : chs) {
          hameg->setCurrent(ch, current);
          std::cout << "Set current of channel " << ch << " to " << current;
          std::cout << std::endl;
        }
      }
      else {
        result = 1;
      }
    }
  }

  if(result != 0) {
    std::cout << "Channel command must be used as:" << std::endl;
    std::cout << "ch <chnumber(s)> [ON/OFF or <value>A/V]" << std::endl;
  }

  return result;
}



void statusCommand(PowerSupply* hameg) {
  powerstatus(hameg);
  for(int ch = 1; ch < 5; ch++) {
    chstatus(hameg, ch);
  }
}

std::vector<std::string> split_by_spaces(std::string s) {
  std::istringstream iss(s);
  std::vector<std::string> result;
  do {
    std::string subs;
    iss >> subs;
    result.push_back(subs);
  } while(iss);

  result.pop_back();  //Remove last element (strange newline element)
  return result;
}


int RunCommand(PowerSupply& hameg, int argc, std::vector<std::string>& argv) {
  //Check if valid command
  if(argc < 2) {
    print_usage(argv[0]);
    return 1;
  }

  //Else: First argument is the command
  std::string command = argv[1];

  //Usage msg
  if(command == "-h" || command == "--help") {
    print_usage(argv[0]);
    return 0;
  }

  //Proper commands
  if(isCommand(command)) {
    //Commands
    if(command == "power") powerCommand(&hameg, argc, argv);
    else if(command == "ch") chCommand(&hameg, argc, argv);
    else if(command == "status") statusCommand(&hameg);
  }
  else {
    print_usage(argv[0]);
    return 1;
  }

  return 0;
}



int main(int argc, char* argv[]) {
  QCoreApplication app(argc, argv);  //Otherwise some strange error msgs appear
  //Convert argv to vector of strings for convenience
  std::vector<std::string> all_args;
  for(int i = 0; i < argc; i++) {
    all_args.push_back(argv[i]);
  }

  //Open serial port
  PowerSupply hameg;
  if(openport(&hameg)) return 1;

  //RUN AS INTERPRETER MODE
  if(argc == 1) {
    std::cout << "Entering interpreter mode, type in commands... ";
    std::cout << "(.q exits the program)" << std::endl;
    std::string input;
    while(input != ".q") {
      std::cout << "cmd > ";
      getline(std::cin, input);
      if(input != ".q") {
        //Artificially prepend program name
        std::vector<std::string> args = split_by_spaces(input);
        args.insert(args.begin(), ""); //Insert pseudo program name
        int nargs = static_cast<int>(args.size());
        RunCommand(hameg, nargs, args);
      }
    }  //END WHILE
  }
  else {  //RUN ONLY ONE COMMAND
    //usleep(100);
    return RunCommand(hameg, argc, all_args);
  }

  return 0;
}
