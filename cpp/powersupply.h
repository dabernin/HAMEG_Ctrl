#ifndef POWERSUPPLY_H
#define POWERSUPPLY_H


#include <QtSerialPort/QtSerialPort>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <string>


//class MainWindow;

class PowerSupply
{
public:
    PowerSupply();
    ~PowerSupply();
    //PowerSupply(MainWindow *par);
    int isOpen;
    QString portName;
    std::string ID;
    QSerialPort serialPort;

    //MainWindow *parent;

    void ps_printf(const char *fmt...);
    int ps_read(char *dest, int maxSize);
    void ps_flush();
    int Open();
    void Close();
    void setVoltage(int channel, double voltage);
    double getVoltage(int channel);
    double measVoltage(int channel);
    void setCurrent(int channel, double current);
    double getCurrent(int channel);
    double measCurrent(int channel);
    void activateOutput(int channel, int state);
    int isActivated(int channel);
    void power(int on);
    int isPowered();
};

#endif // POWERSUPPLY_H
