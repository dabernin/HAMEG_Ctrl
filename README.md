Remote control software for a Hameg HMP4040 Power Supply.

Two different versions are provided, one written in C++ and one in python. The one in python is recommended since it is more stable.
Both versions rely on the serial commands that can be found in the manual:
https://cdn.rohde-schwarz.com/pws/dl_downloads/dl_common_library/dl_manuals/gb_1/h/hmp_serie/HMP_SCPI_ProgrammersManual_en_01.pdf

# (Pre-)Installation
Modify the file `99-hamegpowersupply.rules` according to the vendor and device id of your HMP4040 and put the file into `/etc/udev/rules.d`.
Use `lsusb` and look for a line like
```
Bus 003 Device 003: ID 0403:ed72 Future Technology Devices International, Ltd HAMEG HO720 Serial Port
```
to find your vendor (in the example: 0403) and device (ed72) id.
Reconnect the power supply. Afterwards, the device should be available as `/dev/HAMEG`.

# Python (tested with python 2.7)
Requires pip, which is for instance included in [(ana)conda](https://anaconda.org).  
Optional, but recommended: Setup a new conda environment via `conda create -n HAMEG python=2.7`.  
Install the script and the requirements with `pip install python/`.  
After that, you can use the script by calling `HMP4040.py --help`.

# C++
Requires Qt5. Install with qmake!

