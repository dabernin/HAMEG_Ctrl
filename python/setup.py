from setuptools import setup

setup(
    name='Hameg_Ctrl',
    version='0.0.1',
    author='Daniel Berninghoff',
    author_email='daniel.berninghoff@gmail.com',
    packages=["./"],
    install_requires=[
        'docopt',
        "e4control @ git+https://github.com/burneyy/E4control@0.0.1"
    ],
    scripts=["HMP4040.py"]
)
