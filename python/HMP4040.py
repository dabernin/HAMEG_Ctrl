#!/usr/bin/env python
"""Remote control for Hameg HMP4040 power supply.

Usage:
    HMP4040.py
    HMP4040.py status
    HMP4040.py power [on | off]
    HMP4040.py ch <no> [on | off]  [--V=<voltage>] [--A=<current>]
    HMP4040.py dump <csvfile> [ch <no>]
    HMP4040.py (-h | --help | --version)

Options:
    -h, --help      Show this screen.
    --version       Show version number
    --V=<voltage>   Set voltage.
    --A=<current>   Set current limit.
"""
from __future__ import print_function
import os
from datetime import datetime
from docopt import docopt
from e4control.devices import HMP4040

#Colored string
strOn = '\033[32mON\033[0m'
strOff = '\033[31mOFF\033[0m'

def show_channelstatus(ps, ch):
    outpset = strOn if ps.getEnableOutput(ch) else strOff
    outpmeas = strOn if ps.measEnableOutput(ch) else strOff
    vset = ps.getVoltage(ch)
    vmeas = ps.measVoltage(ch)
    iset = ps.getCurrent(ch)
    imeas = ps.measCurrent(ch)

    if outpset == outpmeas:
        print("CH {no}: {onoff}\tVmeas(set) = {vmeas:.3f}({vset:.3f})V\tImeas(set) = {imeas:.4f}({iset:.4f})A"
              "".format(no=ch, onoff=outpmeas, vmeas=vmeas, vset=vset, imeas=imeas, iset=iset))
    else:
        print("CH {no}: {onoff} (set: {onoffset})\tVmeas(set) = {vmeas:.3f}({vset:.3f})V\tImeas(set) = {imeas:.4f}({iset:.4f})A"
              "".format(no=ch, onoff=outpmeas, onoffset=outpset, vmeas=vmeas, vset=vset, imeas=imeas, iset=iset))


def main():
    args = docopt(__doc__, version='HMP4040 1.0')


    ps = HMP4040("usb", "/dev/HAMEG", -1)


    if args["status"]:
        ps.output(True)

    elif args["power"]:
        if args["on"]:
            ps.enablePower(True)
        elif args["off"]:
            ps.enablePower(False)

        isOn = strOn if ps.getEnablePower() else strOff
        print("Power " + isOn)

    elif args["dump"]:
        if args["ch"]:
            chs = [ int(char) for char in args["<no>"] ]
        else:
            chs = [ 1,2,3,4 ]

        fname = args["<csvfile>"]
        exists = os.path.isfile(fname)
        with open(fname, "a+") as f:
            #Write header
            if not exists:
                f.write("Datetime\tCh\tEnabled\tVset\tVmeas\tIset\tImeas\n")

            for ch in chs:
                f.write(datetime.now().replace(microsecond=0).isoformat())
                f.write("\t")
                f.write(str(ch))
                f.write("\t")
                f.write(str(ps.measEnableOutput(ch)))
                f.write("\t")
                f.write(str(ps.getVoltage(ch)))
                f.write("\t")
                f.write(str(ps.measVoltage(ch)))
                f.write("\t")
                f.write(str(ps.getCurrent(ch)))
                f.write("\t")
                f.write(str(ps.measCurrent(ch)))
                f.write("\n")

    elif args["ch"]:
        chs = [ int(char) for char in args["<no>"] ]
        for ch in chs:
            if ch < 1 or ch > 4:
                raise ValueError("Channel number must be within 1 and 4!")

            if args["on"]:
                ps.enableOutput(ch, True)
            elif args["off"]:
                ps.enableOutput(ch, False)

            if args["--V"]:
                ps.setVoltage(ch, float(args["--V"]))
            if args["--A"]:
                ps.setCurrent(ch, float(args["--A"]))

            show_channelstatus(ps, ch)


    else:
        ps.interaction()





if __name__ == '__main__':
    main()
